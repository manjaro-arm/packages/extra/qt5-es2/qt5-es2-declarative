# Maintainer: Bernhard Landauer <bernhard@manjaro.org>
# Archlinux-Maintainer: Felix Yan
# Contributor: Andrea Scarpino

pkgname=qt5-es2-declarative
_pkgname=qt5-declarative
_basever=5.15.10
pkgver=5.15.10+kde+r29
pkgrel=1
_commit=3e98cdb2780d052fce3d7a3694596a690cd76aca
arch=('x86_64' 'aarch64')
url='https://www.qt.io'
license=('GPL3' 'LGPL3' 'FDL' 'custom')
pkgdesc='Classes for QML and JavaScript languages'
depends=('qt5-es2-base')
makedepends=('git' 'python' 'vulkan-headers')
groups=('qt' 'qt5')
provides=("qt5-declarative=$pkgver")
conflicts=('qtchooser' 'qt5-declarative')
_pkgfqn=${_pkgname/5-/}
source=(git+https://invent.kde.org/qt/qt/$_pkgfqn#commit=$_commit)
sha256sums=('SKIP')

pkgver() {
  cd $_pkgfqn
  echo "$_basever+kde+r"`git rev-list --count v$_basever-lts-lgpl..$_commit` | sed -e 's|+kde+r0||'
}

build() {
  mkdir build && cd build

  qmake ../${_pkgfqn}
  make
}

package() {
  cd build
  make INSTALL_ROOT="$pkgdir" install

  # Symlinks for backwards compatibility
  for b in "$pkgdir"/usr/bin/*; do
    ln -s $(basename $b) "$pkgdir"/usr/bin/$(basename $b)-qt5
  done

  # Drop QMAKE_PRL_BUILD_DIR because reference the build dir
  find "$pkgdir/usr/lib" -type f -name '*.prl' \
    -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

  install -d "$pkgdir"/usr/share/licenses
  ln -s /usr/share/licenses/qt5-base "$pkgdir"/usr/share/licenses/${_pkgname}
}
